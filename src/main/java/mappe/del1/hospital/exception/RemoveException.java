package mappe.del1.hospital.exception;

import java.io.Serializable;

public class RemoveException extends Exception implements Serializable {

    private static final long serialVersionUID = 1L;

    public RemoveException(String message){
        super(message);
    }

}
