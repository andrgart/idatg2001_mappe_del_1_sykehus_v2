package mappe.del1.hospital.person;

/**
 * The interface Diagnosable.
 */
public interface Diagnosable {

    /**
     * Sets diagnose.
     *
     * @param diagnose the diagnose
     */
    void setDiagnose(String diagnose);

}
