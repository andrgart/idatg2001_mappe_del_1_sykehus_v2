package mappe.del1.hospital.person.employee.doctor;

import mappe.del1.hospital.person.Patient;
import mappe.del1.hospital.person.employee.Employee;

/**
 * The type Doctor.
 */
public abstract class Doctor extends Employee {

    /**
     * Instantiates a new Doctor.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets diagnosis.
     *
     * @param patient  the patient
     * @param diagnose the diagnose
     */
    public abstract void setDiagnosis(Patient patient, String diagnose);

}
