package mappe.del1.hospital;

import java.util.ArrayList;

/**
 * The type Hospital.
 */
public class Hospital {

    private final String hospitalName;
    private final ArrayList<Department> departments;

    /**
     * Instantiates a new Hospital.
     *
     * @param hospitalName the hospital name
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        departments = new ArrayList<>();
    }

    /**
     * Gets hospital name.
     *
     * @return the hospital name
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Gets departments.
     *
     * @return the departments
     */
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * Add department.
     *
     * @param department the department
     */
    public void addDepartment(Department department){
        departments.add(department);
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departmentArrayList=" + departments +
                '}';
    }

}
