package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import mappe.del1.hospital.person.Patient;
import mappe.del1.hospital.person.employee.Employee;

/**
 * The type Hospital client.
 */
public class HospitalClient {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        Hospital hospital = new Hospital("Bærum sykehus");
        HospitalTestData.fillRegisterWithTestData(hospital);

        // Setup a department and people to remove.
        Department department = hospital.getDepartments().get(0);
        Employee employeeToRemove = department.getEmployees().get(1);
        Patient patientToRemove = new Patient("Dude", "WhoDoesntExist", "12345678900");
        System.out.println(department.getEmployees());
        System.out.println("Employee to be removed: " + employeeToRemove);

        try {
            // Should succeed.
            department.remove(employeeToRemove);

            // Should fail.
            //department.remove(patientToRemove);

        } catch (RemoveException e) {
            System.err.printf("Could not remove the person! More info: %s", e.getMessage());
        }
        System.out.println("\n" + department.getEmployees());
    }

}
