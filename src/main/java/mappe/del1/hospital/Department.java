package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import mappe.del1.hospital.person.Patient;
import mappe.del1.hospital.person.Person;
import mappe.del1.hospital.person.employee.Employee;

import java.util.*;

/**
 * The type Department.
 */
public class Department {

    private String departmentName;
    private final HashMap<String, Employee> employees;
    private final HashMap<String, Patient> patients;

    /**
     * Instantiates a new Department.
     *
     * @param departmentName the department name
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        employees = new HashMap<>();
        patients = new HashMap<>();
    }

    /**
     * Gets department name.
     *
     * @return the department name
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets department name.
     *
     * @param departmentName the department name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Gets employees.
     *
     * @return the employees
     */
    public List<Employee> getEmployees() {
        return new ArrayList<>(employees.values());
    }

    /**
     * Gets patients.
     *
     * @return the patients
     */
    public List<Patient> getPatients() {
        return new ArrayList<>(patients.values());
    }

    /**
     * Add employee.
     *
     * @param employee the employee
     */
    public void addEmployee(Employee employee){
        employees.putIfAbsent(employee.getSocialSecurityNumber(), employee);
    }

    /**
     * Add patient.
     *
     * @param patient the patient
     */
    public void addPatient(Patient patient){
        patients.put(patient.getSocialSecurityNumber(), patient);
    }

    /**
     * Removes an employee or patient.
     *
     * @param person the person
     * @throws RemoveException the remove exception
     */
    public void remove(Person person) throws RemoveException {

        Person tempPerson;

        if(person instanceof Employee) {
            if(!employees.isEmpty()){
                Employee employee = (Employee) person;
                if (employees.containsValue(person)) {
                    tempPerson = employee;
                }else{
                    throw new RemoveException(String.format("%s does not exist.", person.toString()));
                }
                employees.remove(tempPerson.getSocialSecurityNumber(), tempPerson);
            }else{
                throw new RemoveException("The employee database is empty.");
            }
        }

        if(person instanceof Patient){
            if(!patients.isEmpty()){
                Patient patient = (Patient) person;
                if (patients.containsValue(patient)) {
                    tempPerson = patient;
                }else{
                    throw new RemoveException(String.format("%s does not exist.", person.toString()));
                }
                patients.remove(tempPerson.getSocialSecurityNumber(), tempPerson);
            }else{
                throw new RemoveException("The patient database is empty.");
            }
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employeeArrayList=" + employees +
                ", patientArrayList=" + patients +
                '}';
    }

    /**
     * Print list.
     */
    public void printList(){
        System.out.println(Arrays.asList(employees));
    }

}
