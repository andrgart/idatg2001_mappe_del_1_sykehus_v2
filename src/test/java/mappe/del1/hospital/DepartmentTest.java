package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import mappe.del1.hospital.person.employee.Employee;
import mappe.del1.hospital.person.employee.Nurse;
import mappe.del1.hospital.person.employee.doctor.GeneralPractitioner;
import mappe.del1.hospital.person.employee.doctor.Surgeon;
import mappe.del1.hospital.person.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Department test.
 */
class DepartmentTest {

    /**
     * The Hospital.
     */
    Hospital hospital;
    /**
     * The Department.
     */
    Department department;
    /**
     * The Employee to remove.
     */
    Employee employeeToRemove;


    /**
     * The Patient to remove.
     */
    Patient patientToRemove;


    /**
     * Set up.
     */
    @BeforeEach
    void setUp(){
        hospital = new Hospital("Bærum sykehus");
        HospitalTestData.fillRegisterWithTestData(hospital);
        department = hospital.getDepartments().get(0);
        employeeToRemove = department.getEmployees().get(2);
        patientToRemove = department.getPatients().get(0);
    }

    /**
     * Remove.
     *
     * @throws RemoveException the remove exception
     */
    @Test
    @DisplayName("Removes employee when matched with existing employee.")
    void removeEmployee() throws RemoveException {
        int sizeBefore = department.getEmployees().size();
        department.remove(employeeToRemove);
        int sizeAfter = department.getEmployees().size();
        assertEquals(sizeAfter + 1, sizeBefore);
    }

    /**
     * Remove patient.
     *
     * @throws RemoveException the remove exception
     */
    @Test
    @DisplayName("Removes patient when matched with existing patient.")
    void removePatient() throws RemoveException {
        int sizeBefore = department.getPatients().size();
        department.remove(patientToRemove);
        int sizeAfter = department.getPatients().size();
        assertEquals(sizeAfter + 1, sizeBefore);
    }

    /**
     * Test removing non existing patient.
     */
    @Test
    @DisplayName("Removes a non-existing employee from the list. Throws RemoveException")
    void testRemovingNonExistingEmployee(){
        assertThrows(RemoveException.class, () -> {
            department.remove(new Employee("Ikke", "Eksisterende employee", "050505050505"));
        });
    }

    @Test
    @DisplayName("Removes a non-existing patient from the list. Throws RemoveException")
    void testRemovingNonExistingPatient(){
        assertThrows(RemoveException.class, () -> {
            department.remove(new Patient("Ikke", "Eksisterende patient", "050505050506"));
        });
    }

    @Test
    @DisplayName("Removes a non-existing patient where the info matches an employee. Throws RemoveException")
    void testRemoveNonExistingPatientThatMatchesInfoFromExistingEmployeeThrowsRemoveException(){
        department.addEmployee(new Employee("Hubert", "Hubertson", "13374567654"));
        assertThrows(RemoveException.class, () -> {
            department.remove(new Patient("Hubert", "Hubertson", "13374567654"));
        });
    }

}